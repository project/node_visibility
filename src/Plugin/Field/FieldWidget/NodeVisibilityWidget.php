<?php

namespace Drupal\node_visibility\Plugin\Field\FieldWidget;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'node_visibility' widget.
 *
 * @FieldWidget(
 *   id = "node_visibility",
 *   label = @Translation("Node visibility"),
 *   field_types = {
 *     "node_visibility"
 *   }
 * )
 */
class NodeVisibilityWidget extends WidgetBase {

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected $manager;

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Condition\ConditionManager $manager
   *   The condition plugin manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ConditionManager $manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\Core\Condition\ConditionManager $manager */
    $manager = $container->get('plugin.manager.condition');
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['third_party_settings'], $manager);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element += [
      '#element_validate' => [[static::class, 'validateFormElement']],
    ];

    $element['visibility_tabs'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Visibility'),
      '#parents' => ['visibility_tabs'],
      '#attached' => [
        'library' => [
          'block/drupal.block',
        ],
      ],
    ];

    /** @var \Drupal\node\Entity\Node $entity */
    $entity = $items->getEntity();
    $form_state->set('entity', $entity);

    if (!empty($items->getValue()[0]['value'])) {
      $visibility = $items->getValue()[0]['value'];
    }
    else {
      $items->applyDefaultValue();
      $visibility = $items->getValue()[0]['value'];
    }
    $visibility = Json::decode($visibility);
    $definitions = $this->manager->getFilteredDefinitions('node_visibility', $form_state->getTemporaryValue('gathered_contexts'), ['node' => $entity]);

    $target_conditions = $items->getSetting('target_conditions');
    if ($form_state->getBuildInfo()['form_id'] === 'field_config_edit_form') {
      $user_input = $form_state->getUserInput();
      if (isset($user_input['settings']['target_conditions'])) {
        $target_conditions = [];
        foreach ($user_input['settings']['target_conditions'] as $condition) {
          if ($condition) {
            $target_conditions[] = $condition;
          }
        }
      }
    }

    $element['visibility'] = [];
    foreach ($definitions as $condition_id => $definition) {
      if (!empty($target_conditions) && !in_array($condition_id, $target_conditions)) {
        continue;
      }
      // Don't display the current theme condition.
      if ($condition_id === 'current_theme') {
        continue;
      }
      // Don't display node type condition.
      if ($condition_id === 'node_type') {
        continue;
      }
      // Don't display request path condition.
      if ($condition_id === 'request_path') {
        continue;
      }
      // Don't display entity bundle conditions.
      if (strpos($condition_id, 'entity_bundle') === 0) {
        continue;
      }
      // Don't display the language condition if the node is not translatable.
      if ($condition_id === 'language' && !$entity->isTranslatable()) {
        continue;
      }

      /** @var \Drupal\Core\Condition\ConditionInterface $condition */
      $condition = $this->manager->createInstance($condition_id, $visibility[$condition_id] ?? []);
      $form_state->set(['conditions', $condition_id], $condition);
      $condition_form = $condition->buildConfigurationForm([], $form_state);
      $condition_form['#type'] = 'details';
      $condition_form['#title'] = $condition->getPluginDefinition()['label'];
      $condition_form['#group'] = 'visibility_tabs';
      $element['visibility'][$condition_id] = $condition_form;
    }

    return $element;
  }

  /**
   * Form element validation handler for the visibility form element.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateFormElement(array &$element, FormStateInterface $form_state) {
    $visibility_path = array_merge($element['#parents'], ['visibility']);
    $conditions = $form_state->getValue($visibility_path);
    if ($conditions) {
      foreach ($conditions as $condition_id => $values) {
        // All condition plugins use 'negate' as a Boolean in their schema.
        // However, certain form elements may return it as 0/1. Cast here to
        // ensure the data is in the expected type.
        if (array_key_exists('negate', $values)) {
          $form_state->setValue(
            array_merge($visibility_path, [$condition_id, 'negate']),
            (bool) $values['negate']
          );
        }

        // Allow the condition to validate the form.
        $condition = $form_state->get(['conditions', $condition_id]);
        $condition->validateConfigurationForm($element['visibility'][$condition_id], SubformState::createForSubform($element['visibility'][$condition_id], $element, $form_state));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $result = [];
    foreach ($values[0]['visibility'] as $condition_id => $condition_values) {
      // Allow the condition to submit the form.
      /** @var \Drupal\Core\Condition\ConditionInterface $condition */
      $condition = $form_state->get(['conditions', $condition_id]);

      $field_name = $this->fieldDefinition->getName();
      $key_exists = NULL;
      $condition_path = array_merge(
        $form['#parents'],
        [$field_name, 'widget', 0, 'visibility', $condition_id]
      );
      $condition_form = NestedArray::getValue($form, $condition_path, $key_exists);
      // The form in default value is weird and we have to do weird things.
      if (!$key_exists) {
        $condition_path = ['widget', 0, 'visibility', $condition_id];
        $condition_form = NestedArray::getValue($form, $condition_path, $key_exists);
        array_unshift($condition_form['#parents'], $form['#parents'][0]);
      }
      $condition->submitConfigurationForm($condition_form, SubformState::createForSubform($condition_form, $form, $form_state));

      // Setting conditions' context mappings is the plugins' responsibility.
      // This code exists for backwards compatibility, because
      // \Drupal\Core\Condition\ConditionPluginBase::submitConfigurationForm()
      // did not set its own mappings until Drupal 8.2.
      // @todo Remove the code that sets context mappings in Drupal 9.0.0.
      if ($condition instanceof ContextAwarePluginInterface) {
        $context_mapping = $condition_values['context_mapping'] ?? [];
        $condition->setContextMapping($context_mapping);
      }

      $condition_configuration = $condition->getConfiguration();

      $result[$condition_id] = $condition_configuration;
    }
    return ['value' => Json::encode($result)];
  }

}
