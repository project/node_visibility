<?php

namespace Drupal\node_visibility\Plugin\Field\FieldType;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\MapItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'Node visibility' entity field type.
 *
 * @FieldType(
 *   id = "node_visibility",
 *   label = @Translation("Node visibility"),
 *   description = @Translation("An entity field containing a node visibility."),
 *   cardinality = 1,
 *   default_widget = "node_visibility",
 * )
 */
class NodeVisibilityItem extends MapItem {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function applyDefaultValue($notify = TRUE) {
    // Apply the default value of all properties.
    $this->values = ['value' => '{}'];
    foreach ($this->getProperties() as $key => $property) {
      if ($key === 'value') {
        $property->setValue('{}');
      }
      else {
        $property->applyDefaultValue();
      }
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Serialized values'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'target_conditions' => [],
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Condition\ConditionManager $manager */
    $manager = \Drupal::service('plugin.manager.condition');
    $definitions = $manager->getFilteredDefinitions('node_visibility', $form_state->getTemporaryValue('gathered_contexts'), ['node' => $this->getEntity()]);

    $options = [];
    foreach ($definitions as $condition_id => $definition) {
      // Don't display the current theme condition.
      if ($condition_id === 'current_theme') {
        continue;
      }
      // Don't display node type condition.
      if ($condition_id === 'node_type') {
        continue;
      }
      // Don't display request path condition.
      if ($condition_id === 'request_path') {
        continue;
      }
      // Don't display entity bundle conditions.
      if (strpos($condition_id, 'entity_bundle') === 0) {
        continue;
      }
      // Don't display the language condition if the node is not translatable.
      if ($condition_id === 'language' && !$this->getEntity()->isTranslatable()) {
        continue;
      }
      $options[$condition_id] = $definition['label'];
    }

    $element = [];
    $element['target_conditions'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Conditions'),
      '#options' => $options,
      '#default_value' => $this->getSetting('target_conditions'),
      '#description' => $this->t('Selection of Conditions for this field. Select none to allow all Conditions.'),
      '#ajax' => [
        'callback' => [$this, 'ajaxUpdateDefaultValues'],
        'event' => 'change',
      ],
    ];

    return $element;
  }

  /**
   * Update default values.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AJAX command.
   */
  public static function ajaxUpdateDefaultValues(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('.field--type-node-visibility', $form['default_value']));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public static function fieldSettingsToConfigData(array $settings) {
    $target_conditions = [];
    foreach ($settings['target_conditions'] as $condition) {
      if ($condition) {
        $target_conditions[] = $condition;
      }
    }
    $settings['target_conditions'] = $target_conditions;
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    // A map item has no main property.
    return 'value';
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->values['value']);
  }

}
