# Node Visibility

> This module allows to use the Condition Plugins on nodes like we can use them on Blocks or in Page Manager.
With it, you can define your own Condition Plugins to restrict the access to a node.

It is a field that needs to be added to the content type where you wanna use it, so it can be enabled by specific content types and allows to set default options.

Having the UserRole Condition in core it allows to filter access to nodes by role.

Requirements
============

This module requires Drupal 8 or Drupal 9.
Drupal core node module.
